<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    // protected $table ="film";
    protected $table = "film";
    protected $primaryKey ="film_id";
    public $timestamps = false;
    

    // crear o extender el modelo para relacionarlo con categorias
    //metodo de clase
    public function categorias () {
        return $this->belongsToMany("App\Categoria",
                                     "film_category", 
                                      "film_id",
                                      "category_id");
             }
    public function Idioma(){
        return $this->belongsTo('App\Idioma' , 'language_id');


    }

    public function actores(){
        return $this->belongsToMany("App\Actor",
                                    "film_actor",
                                    "film_id",
                                    "actor_id");
    }
    
}
