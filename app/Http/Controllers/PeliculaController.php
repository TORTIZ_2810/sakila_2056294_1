<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Categoria;

class PeliculaController extends Controller
{
    public function index(){

        $peliculas = Pelicula::paginate(10);
         return view("peliculas.index")
                                ->with("peliculas", $peliculas);

      

    }

    
}
