<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2> Catalogo de peliculas </h2>
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>Título</th>
                <th>Año de Lanzamiento</th>
                <th>Idioma</th>
                <th>Categoría</th>
                <th>Actores</th>
               

            </tr>
        </thead>
        <tbody>
            @foreach ($peliculas as $pelicula): 
            <tr>
                <td>{{ $pelicula->title }}</td>
                <td>{{ $pelicula->release_year}}</td>
                <td>{{ $pelicula->idioma()->first()->name }}</td>
                <td>{{ $pelicula->categorias()->get()->first()->name }}</td>
                <td>
                    @foreach ($peliculas->actores()->get() as $actor )
                    {{  $actor->first_name}} {{ $actor->last_name }} 
                        
                    @endforeach

                </td>
            <tr>
            @endforeach

        </tbody>
    </table>
</body>
</html>